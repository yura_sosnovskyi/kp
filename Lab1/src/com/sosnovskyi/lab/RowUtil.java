package com.sosnovskyi.lab;

import com.sosnovskyi.lab.entity.BigFraction;
import com.sosnovskyi.lab.entity.CommonFraction;
import com.sosnovskyi.lab.entity.Fraction;

import java.math.BigInteger;
import java.util.Scanner;

public class RowUtil {

    public static final int DEFAULT_NUMERATOR = 1;
    public static final int DEFAULT_DENOMINATOR_POWER = 2;
    public static final int MAX_ARG = 16;

    public static Fraction calculate(int n) {
        if (n < 1) {
            throw new IllegalArgumentException("n cannot be less than 1!");
        }

        if (n < MAX_ARG) {
            CommonFraction result = new CommonFraction();
            for (int i = 1; i <= n; i++) {
                result = result.add(new CommonFraction(DEFAULT_NUMERATOR, Math.round(Math.pow(i, 2))));
            }
            return result;
        }

        BigFraction result = new BigFraction();
        for (int i = 1; i <= n; i++) {
            result = result.add(new BigFraction(BigInteger.valueOf(DEFAULT_NUMERATOR),
                                BigInteger.valueOf(Math.round(Math.pow(i, DEFAULT_DENOMINATOR_POWER)))));
        }
        return result;
    }

    public static void demonstrate() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Row: " + DEFAULT_NUMERATOR + " / n^" + DEFAULT_DENOMINATOR_POWER);
        System.out.print("Enter n: ");
        int n = scanner.nextInt();
        System.out.println("Result: " + calculate(n));

        // double view
        double result = 0;
        for (int i = 1; i <= n; i++) {
            result += 1 / Math.pow(i, DEFAULT_DENOMINATOR_POWER);
        }
        System.out.println("In double view: " + result);

    }
}
