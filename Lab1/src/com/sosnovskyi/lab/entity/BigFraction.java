package com.sosnovskyi.lab.entity;

import java.math.BigInteger;

public class BigFraction implements Fraction {

    private BigInteger numerator;
    private BigInteger denominator;

    public BigFraction() {
        this.numerator = BigInteger.valueOf(0);
        this.denominator = BigInteger.valueOf(1);
    }

    public BigFraction(BigInteger numerator, BigInteger denominator) {
        this.numerator = numerator;
        this.denominator = denominator;
        this.simplify();
    }

    @Override
    public String toString() {
        return String.format("(%d / %d)", numerator, denominator);
    }

    public BigFraction add(BigFraction addition) {

        return new BigFraction(numerator.multiply(addition.denominator).add(addition.numerator.multiply(denominator)),
                denominator.multiply(addition.denominator));
    }

    private void simplify() {
        BigInteger divider = gcd(numerator, denominator);
        this.numerator = numerator.divide(divider);
        this.denominator = denominator.divide(divider);
    }

    public static BigInteger gcd(BigInteger a, BigInteger b) {
        return b.compareTo(BigInteger.valueOf(0)) == 0 ? a : gcd(b, a.remainder(b));
    }
}
