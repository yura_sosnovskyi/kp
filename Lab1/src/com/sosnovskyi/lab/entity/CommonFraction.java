package com.sosnovskyi.lab.entity;

public class CommonFraction implements Fraction {

    private long numerator;
    private long denominator;

    public CommonFraction() {
        this.numerator = 0;
        this.denominator = 1;
    }

    public CommonFraction(long numerator, long denominator) {
        this.numerator = numerator;
        this.denominator = denominator;
        this.simplify();
    }

    @Override
    public String toString() {
        return String.format("(%d / %d)", numerator, denominator);
    }

    public CommonFraction add(CommonFraction addition) {
        return new CommonFraction(numerator * addition.denominator + addition.numerator * denominator,
                denominator * addition.denominator);
    }

    private void simplify() {
        long divider = gcd(numerator, denominator);
        this.numerator = numerator / divider;
        this.denominator = denominator / divider;
    }

    public static long gcd(long a, long b) {
        return b == 0 ? a : gcd(b, a % b);
    }
}
