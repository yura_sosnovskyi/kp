package com.sosnovskyi.lab.entity;

public interface Fraction {
    String toString();
}
