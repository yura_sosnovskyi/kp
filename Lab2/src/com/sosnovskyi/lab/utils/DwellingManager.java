package com.sosnovskyi.lab.utils;

import com.sosnovskyi.lab.building.Dwelling;
import com.sosnovskyi.lab.building.impl.Flat;
import com.sosnovskyi.lab.building.impl.Hostel;
import com.sosnovskyi.lab.building.impl.House;

import java.util.*;

public class DwellingManager {

    private static List<Dwelling> dwellingList;

    static class AreaComparator implements Comparator<Dwelling> {

        @Override
        public int compare(Dwelling o1, Dwelling o2) {
            return Double.compare(o1.getArea(), o2.getArea());
        }
    }

    class DistanceComparator implements Comparator<Dwelling>{

        @Override
        public int compare(Dwelling o1, Dwelling o2) {
            return Double.compare(o1.getAvgInfDistance(), o2.getAvgInfDistance());
        }
    }

    public static void initDwellingList() {
        dwellingList = new ArrayList<>();
        dwellingList.add(new House(100, 2, 5, "New house", "Lviv, Shevchenka 120a", 500, 243.5));
        dwellingList.add(new Flat(68, 1, 2, "New flat", "Kyiv, Maidan Nezalezhnosti 12", 1, 379.8));
        dwellingList.add(new House(120, 2, 5, "Intergalbug", "Irpin, Heroiv Upa 23", 500, 245.5));
        dwellingList.add(new House(142, 2, 5, "New house-2", "Lviv, Ozhynova 5", 500, 240));
        dwellingList.add(new Flat(45, 1,  1, "Some flat", "Lviv, Naukova 22", 5, 850.5));
        dwellingList.add(new House(100, 2, 5, "Riel", "Lviv, Zolota 120a", 500, 289.3));
        dwellingList.add(new Flat(65, 1, 2, "Nova oselya", "Lviv, Shevchenka 332", 4, 543.5));
        dwellingList.add(new Hostel(1510, 5, 50, "Block-2", "Kharkiv, Ploshcha 1", 10, 199.7));
        dwellingList.add(new House(100, 2, 5, "Novyi dim", "Lviv, Kolberga 11", 500, 607));
        dwellingList.add(new House(100, 2, 5, "New house", "Lviv, Shevchenka 221a", 500, 402));
        dwellingList.add(new Flat(114, 2, 5, "Two-floor flat", "Lviv, Shevchenka 121", 9, 350));
        dwellingList.add(new House(210, 3, 7, "Smetana", "Ternopil, Kovcha 12", 500, 1008.7));
        dwellingList.add(new House(180, 2, 6, "Oselya", "Lviv, Khmelnytskoho 36", 500, 1439));
        dwellingList.add(new Hostel(1800, 5, 55, "Block-17", "Lviv, Sakharova 55b", 12, 1201));
        dwellingList.add(new Hostel(2250, 6, 72, "Room", "Lviv, Khmelnytskoho 52", 14, 543));
    }

    public static void printDwellingList() {
        dwellingList.forEach(System.out::println);
    }

    public static List<Dwelling> getDwellingList() {
        return dwellingList;
    }

    public static void printSelectedType(Class o) {
        dwellingList.stream().filter(o::isInstance).forEach(System.out::println);
    }

    public static void printLvivDwellings() {
        dwellingList.stream().filter(x -> x.getAddress().contains("Lviv")).forEach(System.out::println);
    }

    public static void sortByDescription(Order order){
        if (order.equals(Order.ASCENDING)){
            dwellingList.sort(new Comparator<Dwelling>() {
                @Override
                public int compare(Dwelling o1, Dwelling o2) {
                    return String.CASE_INSENSITIVE_ORDER.compare(o1.getDescription(),
                            o2.getDescription());
                }
            });
        } else {
            dwellingList.sort(new Comparator<Dwelling>() {
                @Override
                public int compare(Dwelling o1, Dwelling o2) {
                    return -String.CASE_INSENSITIVE_ORDER.compare(o1.getDescription(),
                            o2.getDescription());
                }
            });
        }
    }

    public static void sortByRoomsCount(Order order){
        Comparator<Dwelling> countryComparator = (o1, o2) -> Integer.compare(o1.getRoomsCount(), o2.getRoomsCount());
        if (order.equals(Order.ASCENDING)){
            dwellingList.sort(countryComparator);
        } else {
            dwellingList.sort(countryComparator.reversed());
        }
    }

    public static void sortByArea(Order order){
        if (order.equals(Order.ASCENDING)){
            dwellingList.sort(new AreaComparator());
        } else {
            dwellingList.sort(new AreaComparator().reversed());
        }
    }

    public static void sortByDistance(Order order){

        if (order.equals(Order.ASCENDING)){
            dwellingList.sort(new DwellingManager().new DistanceComparator());
        } else {
            dwellingList.sort(new DwellingManager().new DistanceComparator().reversed());
        }
    }
}
