package com.sosnovskyi.lab.utils;

public enum Order {
    DESCENDING, ASCENDING
}
