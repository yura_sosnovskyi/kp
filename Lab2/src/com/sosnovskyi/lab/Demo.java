package com.sosnovskyi.lab;

import com.sosnovskyi.lab.building.impl.Flat;
import com.sosnovskyi.lab.building.impl.Hostel;
import com.sosnovskyi.lab.building.impl.House;
import com.sosnovskyi.lab.utils.DwellingManager;
import com.sosnovskyi.lab.utils.Order;

/*
Житло Визначити ієрархію житла, наприклад: особняк, квартира, пентхаус і т.д.
Сформувати пропозиції наявного житла в м. Львові для покупця.
Врахувати розташування житла до об’єктів соціальної інфраструктури (садочки, школи, дитячі майданчики)
Реалізувати можливість  сортування знайдених опцій житла за двома типами параметрів (на вибір, реалізовано як два окремі методи)
Реалізація сортування має передбачати можливість сортувати як за спаданням, так і за зростанням
 */

public class Demo {

    public static void main(String[] args) {

        System.out.println("All Dwellings:");
        DwellingManager.initDwellingList();
        DwellingManager.printDwellingList();

        System.out.println(System.lineSeparator() + "Dwellings sorted by description:");
        DwellingManager.sortByDescription(Order.ASCENDING);
        DwellingManager.printDwellingList();

        System.out.println(System.lineSeparator() + "Dwellings sorted by rooms count:");
        DwellingManager.sortByRoomsCount(Order.ASCENDING);
        DwellingManager.printDwellingList();

        System.out.println(System.lineSeparator() + "Dwellings sorted by area:");
        DwellingManager.sortByArea(Order.ASCENDING);
        DwellingManager.printDwellingList();

        System.out.println(System.lineSeparator() + "Dwellings sorted by infrastructure distance:");
        DwellingManager.sortByDistance(Order.ASCENDING);
        DwellingManager.printDwellingList();

        System.out.println(System.lineSeparator() + "Houses:");
        DwellingManager.printSelectedType(House.class);

        System.out.println(System.lineSeparator() + "Flats:");
        DwellingManager.printSelectedType(Flat.class);

        System.out.println(System.lineSeparator() + "Hostels:");
        DwellingManager.printSelectedType(Hostel.class);

        System.out.println(System.lineSeparator() + "Dwellings located in Lviv:");
        DwellingManager.printLvivDwellings();


    }
}