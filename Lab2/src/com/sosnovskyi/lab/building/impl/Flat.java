package com.sosnovskyi.lab.building.impl;

import com.sosnovskyi.lab.building.Dwelling;

public class Flat extends Dwelling {
    private int startingFloor;

    public Flat(double area, int floorsCount, int roomsCount, String description,
                String address, int startingFloor, double avgInfDistance) {
        super(area, floorsCount, roomsCount, description, address, avgInfDistance);
        this.startingFloor = startingFloor;
    }

    public int getStartingFloor() {
        return startingFloor;
    }

    public void setStartingFloor(int startingFloor) {
        this.startingFloor = startingFloor;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Flat)) return false;
        if (!super.equals(o)) return false;

        Flat flat = (Flat) o;

        if (Double.compare(flat.area, area) != 0) return false;
        if (floorsCount != flat.floorsCount) return false;
        if (roomsCount != flat.roomsCount) return false;
        if (Double.compare(flat.avgInfDistance, avgInfDistance) != 0) return false;
        if (!description.equals(flat.description)) return false;

        return startingFloor == flat.startingFloor
                && address.equals(flat.address);
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + startingFloor;
        return result;
    }

    @Override
    public String toString() {
        return "Flat{" +
                "area=" + area +
                ", floorsCount=" + floorsCount +
                ", roomsCount=" + roomsCount +
                ", description='" + description + '\'' +
                ", address='" + address + '\'' +
                ", avgInfDistance=" + avgInfDistance +
                ", startingFloor=" + startingFloor +
                '}';
    }
}
