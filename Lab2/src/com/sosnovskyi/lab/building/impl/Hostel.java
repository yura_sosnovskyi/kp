package com.sosnovskyi.lab.building.impl;

import com.sosnovskyi.lab.building.Dwelling;

public class Hostel extends Dwelling {
    private int blocksCount;

    public Hostel(double area, int floorsCount, int roomsCount, String description,
                  String address, int blocksCount,  double avgInfDistance) {
        super(area, floorsCount, roomsCount, description, address, avgInfDistance);
        this.blocksCount = blocksCount;
    }

    public int getBlocksCount() {
        return blocksCount;
    }

    public void setBlocksCount(int blocksCount) {
        this.blocksCount = blocksCount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Hostel)) return false;
        if (!super.equals(o)) return false;

        Hostel hostel = (Hostel) o;

        if (Double.compare(hostel.area, area) != 0) return false;
        if (floorsCount != hostel.floorsCount) return false;
        if (roomsCount != hostel.roomsCount) return false;
        if (Double.compare(hostel.avgInfDistance, avgInfDistance) != 0) return false;
        if (!description.equals(hostel.description)) return false;

        return blocksCount == hostel.blocksCount
                && address.equals(hostel.address);
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + blocksCount;
        return result;
    }

    @Override
    public String toString() {
        return "Hostel{" +
                "area=" + area +
                ", floorsCount=" + floorsCount +
                ", roomsCount=" + roomsCount +
                ", description='" + description + '\'' +
                ", address='" + address + '\'' +
                ", avgInfDistance=" + avgInfDistance +
                ", blocksCount=" + blocksCount +
                '}';
    }
}
