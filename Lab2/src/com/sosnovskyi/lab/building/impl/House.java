package com.sosnovskyi.lab.building.impl;

import com.sosnovskyi.lab.building.Dwelling;

import java.util.Objects;

public class House extends Dwelling {
    private double yardArea;

    public House(double area, int floorsCount, int roomsCount, String description,
                 String address, double yardArea, double avgInfDistance) {
        super(area, floorsCount, roomsCount, description, address, avgInfDistance);
        this.yardArea = yardArea;
    }

    public double getYardArea() {
        return yardArea;
    }

    public void setYardArea(double yardArea) {
        this.yardArea = yardArea;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        House house = (House) o;

        if (Double.compare(house.area, area) != 0) return false;
        if (floorsCount != house.floorsCount) return false;
        if (roomsCount != house.roomsCount) return false;
        if (Double.compare(house.avgInfDistance, avgInfDistance) != 0) return false;
        if (!description.equals(house.description)) return false;

        return Double.compare(house.yardArea, yardArea) == 0
                && address.equals(house.address);
    }

    @Override
    public int hashCode() {
        return Objects.hash(yardArea);
    }

    @Override
    public String toString() {
        return "House{" +
                "area=" + area +
                ", floorsCount=" + floorsCount +
                ", roomsCount=" + roomsCount +
                ", description='" + description + '\'' +
                ", address='" + address + '\'' +
                ", avgInfDistance=" + avgInfDistance +
                ", yardArea=" + yardArea +
                '}';
    }
}
