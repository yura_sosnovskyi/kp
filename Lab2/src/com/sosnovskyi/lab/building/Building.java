package com.sosnovskyi.lab.building;

public interface Building {
    double getArea();
    double getAvgInfDistance();
    int getFloorsCount();
    int getRoomsCount();
    String getAddress();
    String getDescription();
}
