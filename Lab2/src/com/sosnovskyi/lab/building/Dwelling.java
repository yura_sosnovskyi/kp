package com.sosnovskyi.lab.building;

public abstract class Dwelling implements Building {

    protected double area;
    protected int floorsCount;
    protected int roomsCount;
    protected String description;
    protected String address;
    protected double avgInfDistance;

    public Dwelling(double area, int floorsCount, int roomsCount,
                    String description, String address, double avgInfDistance) {
        this.area = area;
        this.floorsCount = floorsCount;
        this.roomsCount = roomsCount;
        this.description = description;
        this.address = address;
        this.avgInfDistance = avgInfDistance;
    }

    public Dwelling() {
        this.area = 0;
        this.floorsCount = 0;
        this.roomsCount = 0;
        this.avgInfDistance = 0;
        this.description = null;
        this.address = null;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Dwelling dwelling = (Dwelling) o;

        if (Double.compare(dwelling.area, area) != 0) return false;
        if (floorsCount != dwelling.floorsCount) return false;
        if (roomsCount != dwelling.roomsCount) return false;
        if (Double.compare(dwelling.avgInfDistance, avgInfDistance) != 0) return false;
        if (!description.equals(dwelling.description)) return false;
        return address.equals(dwelling.address);
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        temp = Double.doubleToLongBits(area);
        result = (int) (temp ^ (temp >>> 32));
        result = 31 * result + floorsCount;
        result = 31 * result + roomsCount;
        result = 31 * result + description.hashCode();
        result = 31 * result + address.hashCode();
        temp = Double.doubleToLongBits(avgInfDistance);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }

    @Override
    public double getAvgInfDistance() {
        return avgInfDistance;
    }

    public void setAvgInfDistance(double avgInfDistance) {
        this.avgInfDistance = avgInfDistance;
    }

    public void setArea(double area) {
        this.area = area;
    }

    public void setFloorsCount(int floorsCount) {
        this.floorsCount = floorsCount;
    }

    public void setRoomsCount(int roomsCount) {
        this.roomsCount = roomsCount;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public double getArea() {
        return area;
    }

    @Override
    public int getFloorsCount() {
        return floorsCount;
    }

    @Override
    public int getRoomsCount() {
        return roomsCount;
    }

    @Override
    public String getAddress() {
        return address;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public String toString() {
        return "Dwelling{" +
                "area=" + area +
                ", floorsCount=" + floorsCount +
                ", roomsCount=" + roomsCount +
                ", description='" + description + '\'' +
                ", address='" + address + '\'' +
                '}';
    }
}
