package com.sosnovskyi.lab.util;

import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.NoSuchElementException;

import static org.junit.jupiter.api.Assertions.*;

class TextManagerTest {

    @Test
    void makeReplaceTest() {
        assertEquals("fractral actual In." + System.lineSeparator(),
                TextManager.makeReplace("In actual fractral."));
    }

    @Test
    void findPunctuationStopsTest() {
        assertLinesMatch(List.of(".", "?"),
                TextManager.findPunctuationStops("In actual fractral. Kek?"));
    }

    @Test
    void swapTest() {
        assertEquals("Lorem simply is Ipsum dummy text",
                TextManager.swap("Lorem Ipsum is simply dummy text", "simply", "Ipsum"));
    }

    @Test
    void makeReplaceExceptionTest() {
        assertThrows(NoSuchElementException.class,
                ()-> TextManager.makeReplace("In actual fractral"));
    }
}