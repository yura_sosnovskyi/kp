package com.sosnovskyi.lab;

import com.sosnovskyi.lab.util.TextManager;

import java.util.NoSuchElementException;

public class Demo {

    public static void main(String[] args) {
        String result;
        while (true) {
            try {
                result = TextManager.makeReplace(TextManager.readInputText());
                System.out.println(System.lineSeparator() + "Modified text:");
                System.out.println(result);
                break;
            } catch (NoSuchElementException e) {
                System.out.println(e.getMessage());
            }
        }
    }

    /*
    v-10
Lorem Ipsum is simply dummy text of the printing and typesetting industry Lorem Ipsum has been
the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley
of type and scrambled it to make a type specimen book!
It has survived...
Not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.
It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages,
and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum?

     */
}