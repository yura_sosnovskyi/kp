package com.sosnovskyi.lab.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TextManager {

    private TextManager() {
    }

    public static String readInputText() {
        System.out.println(System.lineSeparator()
                + "Please, enter your text:");
        StringBuilder sb = new StringBuilder();
        Scanner scanner = new Scanner(System.in);

        while (scanner.hasNextLine()) {
            String line = scanner.nextLine();
            if (line.equals(""))
                break;
            sb.append(line);
        }

        return sb.toString();
    }

    public static String makeReplace(String input) throws NoSuchElementException {
        if (input.length() < 2 || (!input.contains(".") && !input.contains("?")
                && !input.contains("!") && !input.contains(";"))) {
            throw new NoSuchElementException(
                    "Text should contain at least 1 sentence ending with [.?!;] and should be empty!");
        }
        String[] sentences = input.split("[.?!¿¡;]+");

        var stops = findPunctuationStops(input);
        StringBuilder sb = new StringBuilder();
        String[] words;
        Pattern pattern = Pattern.compile("\\b([AEIOUaeiou]\\w*)");
        Matcher matcher;
        String sentence;

        for (int i = 0; i < sentences.length; i++) {
            sentence = sentences[i];
            words = sentence.split("\\W+");
            matcher = pattern.matcher(sentence);
            if (!matcher.find()) {
                throw new NoSuchElementException(
                        "Sentence: \"" + sentence +
                        "\" should contain at least 1 word starting with vowel");
            }
            sentence = swap(sentence,
                    Arrays.stream(words).max(Comparator.comparing(String::length)).orElseThrow(),
                    matcher.group(1));
            sb.append(sentence.trim()).append(stops.get(i)).append(System.lineSeparator());
        }

        return sb.toString();
    }

    public static List<String> findPunctuationStops(String input) {
        Pattern pattern = Pattern.compile("[.?!¿¡;]+");
        Matcher matcher = pattern.matcher(input);
        List<String> stops = new ArrayList<>();
        while (matcher.find()) {
            stops.add(matcher.group());
        }
        return stops;
    }

    public static String swap(String source, String str1, String str2) {
        source = source.replace(str1, str2);
        source = source.replaceFirst(str2, str1);
        return source;
    }
}

