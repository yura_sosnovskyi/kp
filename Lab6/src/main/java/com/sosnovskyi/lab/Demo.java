package com.sosnovskyi.lab;

import com.sosnovskyi.lab.pizzeria.Pizza;
import com.sosnovskyi.lab.pizzeria.Pizzeria;

import java.io.*;
import java.util.List;
import java.util.Scanner;

/*
Створити класи для роботи піцерії.
Клас Піцца повинен містити інформацію про назву, вагу, вартість та список складників.
Клас Відвідувач піцерії знатиме номер, адресу доставки, також список замовлень та бажаний час доставки для кожного замовлення.
Клас Піцерія містить колекцію всіх піц та колекцію всіх користувачів, а також методи, які з цими колекціями можна здійснювати.

1. Відсортувати всі замовлення  за часом доставки.
2. Створити список адрес для користувачів, що замовили  більше ніж 2 піци.
3. Перевірити, скільки користувачів замовили піцу з заданою назвою.
4. Знайти найбільшу кількість піц, замовлених користувачем серед запропонованого переліку піц.
5. Створити колекцію з переліком піц та списками їх замовників.
6. Створити список не виконаних замовлень на біжучий час, та вказати час перетермінування.
*/
public class Demo {

    public static final String INPUT_PATH_PIZZAS = "pizzasData.json";
    public static final String INPUT_PATH_CLIENTS = "clientsData.json";
    public static final String PATH_SERIALIZE = "pizzeria.dat";

    public static void main(String[] args) {
        Pizzeria pizzeria = new Pizzeria();
        Scanner sc = new Scanner(System.in);
        int option;
        while (true) {
            System.out.print("\n\n1 - Sort all orders by time\n" +
                    "2 - Create address list for users, ordered more than 2 pizzas\n" +
                    "3 - Amount of users have ordered a pizza with a given name\n" +
                    "4 - The largest number of pizzas ordered by the user among the specified list of pizzas\n" +
                    "5 - Collection with pizzas and lists of their customers\n" +
                    "6 - List of unfulfilled orders for the current time, and indicate the time of overdue\n" +
                    "7 - Init pizzeria\n" +
                    "8 - Serialize pizzeria\n" +
                    "9 - Init by deserialization pizzeria\n" +
                    "Another digit - Exit\n" +
                    "Enter your option: ");
            option = sc.nextInt();

            if (option == 1) {
                pizzeria.getSortedOrders().forEach(System.out::println);

            } else if (option == 2) {
                pizzeria.getAddresses().forEach(System.out::println);

            } else if (option == 3) {
                System.out.println("Amount of users ordered Margherita: "
                        + pizzeria.getClientsCount("Margherita"));

            } else if (option == 4) {
                System.out.println("The largest number of pizzas: "
                        + pizzeria.getMaxPizzaCount(pizzeria.getClientList().get(2),
                        List.of(new Pizza("Napoletana", 400.0, 10.0,
                                        List.of("dough", "oregano", "mozzarella", "anchovies")),
                                new Pizza("Pepperoni", 500.0, 10.0,
                                        List.of("dough", "tomato", "cheese", "beacon"))
                        )));

            } else if (option == 5) {
                pizzeria.getPizzasClients().forEach((key, value) -> {
                    System.out.println(key.getName()
                            + " : ");
                    value.forEach(System.out::println);
                });

            } else if (option == 6) {
                pizzeria.getUnfulfilledOrders().forEach((key, value) -> {
                    long diffSeconds = value / 1000 % 60;
                    long diffMinutes = value / (60 * 1000) % 60;
                    long diffHours = value / (60 * 60 * 1000) % 24;
                    long diffDays = value / (24 * 60 * 60 * 1000);
                    System.out.println(key + "\nwith overdue: ");
                    System.out.println(diffDays + " days, "
                            + diffHours + " hours, "
                            + diffMinutes + " minutes, "
                            + diffSeconds + " seconds.\n");
                });

            } else if (option == 7) {
                pizzeria.initPizzeria(INPUT_PATH_PIZZAS, INPUT_PATH_CLIENTS);

            } else if (option == 8) {
                try(ObjectOutputStream oos = new ObjectOutputStream(
                        new FileOutputStream(PATH_SERIALIZE))) {
                    oos.writeObject(pizzeria);
                    System.out.println("Pizzeria was written!");
                } catch (IOException e) {
                    System.err.println(e.getMessage());
                }

            } else if (option == 9) {
                try(ObjectInputStream ois = new ObjectInputStream(
                        new FileInputStream(PATH_SERIALIZE))) {
                    pizzeria = (Pizzeria) ois.readObject();
                    System.out.println("Pizzeria was read!");
                } catch (IOException | ClassNotFoundException e) {
                    System.err.println(e.getMessage());
                }

            } else {
                break;
            }
        }
    }
}
