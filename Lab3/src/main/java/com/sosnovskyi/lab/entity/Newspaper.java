package com.sosnovskyi.lab.entity;

import com.sosnovskyi.lab.utils.Periodicity;

import java.util.Objects;

public class Newspaper {
    private String name;
    private int foundedYear;
    private int printingCount;
    private Periodicity periodicity;
    private String publishingHouse;

    public Newspaper () {};
    public Newspaper(String name, int foundedYear, int printingCount,
                     Periodicity periodicity, String publishingHouse) {
        this.name = name;
        this.foundedYear = foundedYear;
        this.printingCount = printingCount;
        this.periodicity = periodicity;
        this.publishingHouse = publishingHouse;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Newspaper)) return false;

        Newspaper newspaper = (Newspaper) o;

        if (foundedYear != newspaper.foundedYear) return false;
        if (printingCount != newspaper.printingCount) return false;
        if (!Objects.equals(name, newspaper.name)) return false;
        if (periodicity != newspaper.periodicity)
            return false;
        return Objects.equals(publishingHouse, newspaper.publishingHouse);
    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + foundedYear;
        result = 31 * result + printingCount;
        result = 31 * result + (periodicity != null ? periodicity.hashCode() : 0);
        result = 31 * result + (publishingHouse != null ? publishingHouse.hashCode() : 0);
        return result;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getFoundedYear() {
        return foundedYear;
    }

    public void setFoundedYear(int foundedYear) {
        this.foundedYear = foundedYear;
    }

    public int getPrintingCount() {
        return printingCount;
    }

    public void setPrintingCount(int printingCount) {
        this.printingCount = printingCount;
    }

    public Periodicity getPeriodicity() {
        return periodicity;
    }

    public void setPeriodicity(Periodicity periodicity) {
        this.periodicity = periodicity;
    }

    public String getPublishingHouse() {
        return publishingHouse;
    }

    public void setPublishingHouse(String publishingHouse) {
        this.publishingHouse = publishingHouse;
    }

    @Override
    public String toString() {
        return "\nNewspaper{" +
                "name='" + name + '\'' +
                ", foundedYear=" + foundedYear +
                ", printingCount=" + printingCount +
                ", periodicity=" + periodicity +
                ", publishingHouse='" + publishingHouse + '\'' +
                '}';
    }
}
