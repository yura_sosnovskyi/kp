package com.sosnovskyi.lab.utils;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sosnovskyi.lab.entity.Newspaper;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.*;

import static java.util.stream.Collectors.*;

public class NewspaperManager {

    public static final String INPUT_PATH_MAIN = "inputData.json";
    public static final String INPUT_PATH_SECOND = "inputData2.json";
    public static final String INPUT_PATH_THIRD = "inputData3.json";

    private static List<Newspaper> newspaperList;

    public static void fillNewspaperList(String path) {
        if (newspaperList == null)
            newspaperList = new ArrayList<>();
        ObjectMapper mapper = new ObjectMapper();
        ClassLoader classLoader = NewspaperManager.class.getClassLoader();

        try {
            URL resource = classLoader.getResource(path);
            newspaperList.addAll(mapper.readValue(new File(resource.toURI()), new TypeReference<>(){}));
            newspaperList = newspaperList.stream().distinct().collect(toList());
        } catch (IOException | URISyntaxException e) {
            e.printStackTrace();
        }
    }

    public static Map<String, List<String>> firstTask (int minRemoveCount) {
        // creating map
        Map<String, List<String>> namesOfHouses = newspaperList.stream().collect(groupingBy(
                Newspaper::getPublishingHouse, mapping(Newspaper::getName, toList())));
        System.out.println(namesOfHouses);

        //swap indexes
        namesOfHouses.forEach((key, x) -> swapIndexes(x));
        System.out.println(namesOfHouses);

        // deletion
        namesOfHouses.entrySet().removeIf(x -> x.getValue().size() < minRemoveCount);

        return namesOfHouses;
    }

    public static Map<Integer, Integer> secondTask() {
        Map<Integer, Integer> freqCharacteristic = new HashMap<>();
        newspaperList.forEach(key -> freqCharacteristic.merge(key.getPrintingCount(), 1, Integer::sum));
        return freqCharacteristic;
    }

    public static List<Newspaper> thirdTask() {
        newspaperList = null;
        fillNewspaperList(INPUT_PATH_SECOND);
        fillNewspaperList(INPUT_PATH_THIRD);
        newspaperList.sort(Comparator.comparing(Newspaper::getName).reversed());
        newspaperList = List.copyOf(newspaperList);
        return newspaperList;
    }

    private static void swapIndexes(List<String> list) {
        String temp;
        for (int j = 1; j < list.size(); j += 2) {
            temp = list.get(j-1);
            list.set(j-1, list.get(j));
            list.set(j, temp);
        }
    }
}
