package com.sosnovskyi.lab.utils;

public enum Periodicity {
    DAILY, WEEKLY, MONTHLY, QUARTERLY;
}
