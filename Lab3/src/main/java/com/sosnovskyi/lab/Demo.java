package com.sosnovskyi.lab;


import com.sosnovskyi.lab.utils.NewspaperManager;

import java.util.Scanner;

import static com.sosnovskyi.lab.utils.NewspaperManager.INPUT_PATH_MAIN;

public class Demo {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        while (true) {
            System.out.print("\n\ntask1 - 1\n" +
                    "task2 - 2\n" +
                    "task3 - 3\n" +
                    "fill newspaper list - 9\n" +
                    "exit - another digit\n" +
                    "Enter your option: ");
            int option = sc.nextInt();

            if (option == 1) {
                System.out.print("Enter count to remove: ");
                int count = sc.nextInt();
                System.out.println("\nFirst task (map, swap, delete):");
                System.out.println(NewspaperManager.firstTask(count));
            } else if (option == 2) {
                System.out.println("\nSecond task (frequency characteristic):\n" + NewspaperManager.secondTask());
            } else if (option == 3) {
                System.out.println("\nThird task (read, sort):\n" + NewspaperManager.thirdTask());
            } else if (option == 9) {
                NewspaperManager.fillNewspaperList(INPUT_PATH_MAIN);
            } else {
                break;
            }
        }
    }
}
