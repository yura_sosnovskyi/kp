package com.sosnovskyi.lab.dictionary;

import java.io.File;
import java.io.FileNotFoundException;
import java.text.BreakIterator;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeSet;

public class DictionaryManager {

    private DictionaryManager() {
    }

    public static Set<String> scanDictionary(String filename) {
        Set<String> dictionary = new HashSet<>();
        File file = new File(filename);

        try (Scanner scanner = new Scanner(file)) {
            while (scanner.hasNextLine()) {
                String word = scanner.nextLine();
                dictionary.add(word);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        return dictionary;
    }

    public static Set<String> findCompoundWords(Set<String> fullDictionary) {
        Set<String> compoundWords = new HashSet<>();

        for (String word : fullDictionary) {
            for (int i = 0; i < word.length(); i++) {
                if (fullDictionary.contains(word.substring(0, i + 1))
                        && fullDictionary.contains(word.substring(i + 1))) {
                    compoundWords.add(word);
                    break;
                }
            }
        }

        return compoundWords;
    }

    public static String replaceCompoundWords(String text, Set<String> compoundWords) {
        StringBuilder sb = new StringBuilder();

        BreakIterator iterator = BreakIterator.getSentenceInstance();
        iterator.setText(text);
        int start = iterator.first();
        String sentence;
        List<String> sentenceWords;
        for (int end = iterator.next(); end != BreakIterator.DONE; start = end, end = iterator.next()) {
            sentence = text.substring(start, end);
            sentenceWords = getWords(sentence);
            for (var word : sentenceWords) {
                sentence = sentence.replace(word, compoundWords.contains(word) ?
                        sentenceWords.get(0).toLowerCase() : word);
            }
            sb.append(sentence);
        }

        return sb.toString();
    }

    public static List<String> getWords(String text) {
        List<String> words = new ArrayList<>();
        BreakIterator iterator = BreakIterator.getWordInstance();
        iterator.setText(text);
        int start = iterator.first();

        for (int end = iterator.next(); end != BreakIterator.DONE; start = end, end = iterator.next()) {
            if (Character.isLetterOrDigit(text.charAt(start))) {
                words.add(text.substring(start, end));
            }
        }

        return words;
    }
}
