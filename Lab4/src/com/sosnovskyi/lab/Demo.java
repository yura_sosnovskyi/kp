package com.sosnovskyi.lab;

import com.sosnovskyi.lab.dictionary.DictionaryManager;

import java.util.Set;

public class Demo {

    public static final String FILE_NAME = "dictionary.txt";

    public static void main(String[] args) {
        Set<String> dictionary = DictionaryManager.scanDictionary(FILE_NAME);
        Set<String> compoundWords = DictionaryManager.findCompoundWords(dictionary);

        String text = "Hello world. My airplane is really perfect. " +
                "Does anybody wants another cup of coffee? Put this letter in my mailbox... " +
                "Гей догори зелено-білі прапори!";
        String result = DictionaryManager.replaceCompoundWords(text, compoundWords);

        System.out.println(text);
        System.out.println(result);
    }
}